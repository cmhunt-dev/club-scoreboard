# Club cricket scoreboard

## Description
This displays the live scores/results for a club on a particular day.

Currently for internal use only as admin form required to take in API key.

In future, this will be replaced by an admin screen to enter it.

## Install
```
cd src
yarn
yarn build
```

## Run
```
cd src
serve -s build
```