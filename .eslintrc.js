module.exports = {
    extends: ['eslint:recommended', 'prettier', "plugin:react/recommended"], // extending recommended config and config derived from eslint-config-prettier
    plugins: [
      'prettier',
      'eslint-plugin-react'
    ], // activating esling-plugin-prettier (--fix stuff)
    rules: {
      'prettier/prettier': [ // customizing prettier rules (unfortunately not many of them are customizable)
        'error',
        {
          singleQuote: true, 
          trailingComma: 'all',
        },
      ],
      eqeqeq: ['error', 'always'], // adding some custom ESLint rules
    },
    parser: "babel-eslint",
    parserOptions: {
      ecmaVersion: 6,
      sourceType: "module",
      ecmaFeatures: {
        jsx: true,
        modules: true,
        experimentalObjectRestSpread: true
      }
    },
  };