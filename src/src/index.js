import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import App from './components/App';
import store from './store/store';
import config from './config';
import { updateApiKey, setConfig, setMatchRefresh } from './store/actions/app';

store.dispatch(updateApiKey(config.apiKey));
store.dispatch(setConfig(config));
store.dispatch(setMatchRefresh(60000));
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
