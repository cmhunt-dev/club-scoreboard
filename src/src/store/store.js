import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { createLogger } from 'redux-logger';
import app from './reducers/app';

const logger = createLogger({
  diff: true,
  duration: true,
});

export default createStore(
  combineReducers({
    ...app,
  }),
  applyMiddleware(thunk, logger),
);
