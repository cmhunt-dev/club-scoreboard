const appReducer = (state = null, action) => {
  switch (action.type) {
    case 'SET_API_KEY':
      return { ...state, apiKey: action.payload };
    case 'SET_CONFIG':
      return { ...state, config: action.payload };
    case 'SET_MATCH_REFRESH':
      return { ...state, matchRefresh: action.payload };
    default:
      return state;
  }
};

const dataInitialState = {
  matches: [],
  scorecards: {},
};

const dataReducer = (state = dataInitialState, action) => {
  switch (action.type) {
    case 'SET_MATCHES':
      return { ...state, matches: action.payload };
    case 'SET_SCORECARD': {
      let scorecards = Object.assign({}, state.scorecards);
      scorecards[action.payload.matchId] = action.payload.scorecard;
      return { ...state, scorecards };
    }
    default:
      return state;
  }
};

module.exports = {
  app: appReducer,
  data: dataReducer,
};
