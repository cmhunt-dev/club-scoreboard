import {
  fetchMatches as fetchMatchesFromAPI,
  setApiKey,
  fetchScorecard as fetchScorecardFromAPI,
} from '../../lib/api';

export const updateApiKey = apiKey => {
  setApiKey(apiKey);
  return {
    type: 'SET_API_KEY',
    payload: apiKey,
  };
};

export const setConfig = config => ({
  type: 'SET_CONFIG',
  payload: config,
});

export const setMatchRefresh = time => ({
  type: 'SET_MATCH_REFRESH',
  payload: time,
});

export const fetchMatches = () => dispatch => {
  const response = fetchMatchesFromAPI();
  response.then(matches => {
    dispatch({
      type: 'SET_MATCHES',
      payload: matches.matches,
    });
  });
};

export const fetchScorecard = matchId => dispatch => {
  const response = fetchScorecardFromAPI(matchId);
  response.then(scorecard => {
    dispatch({
      type: 'SET_SCORECARD',
      payload: {
        matchId,
        scorecard: scorecard.match_details[0],
      },
    });
  });
};
