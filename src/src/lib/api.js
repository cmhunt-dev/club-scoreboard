import config from '../config';

let apiKey = null;

export function setApiKey(key) {
  apiKey = key;
}

export function fetchMatches() {
  const url = new URL(`${config.apiBaseUrl}/matches.json`);
  const params = {
    site_id: config.clubId,
    season: config.season,
    api_token: apiKey,
  };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  return fetch(url).then(response => response.json());
}

export function fetchScorecard(matchId) {
  const url = new URL(`${config.apiBaseUrl}/match_detail.json`);
  const params = {
    match_id: matchId,
    api_token: apiKey,
  };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  return fetch(url).then(response => response.json());
}
