import { transformMatch } from './matches';

class Scorecard {
  constructor(scorecard, clubId) {
    this.clubId = clubId;
    this.scorecard = transformMatch(scorecard, clubId);
  }

  get() {
    return this.scorecard;
  }

  getBestBowling(no) {
    let innings = null;
    for (let inn of this.scorecard.innings) {
      if (inn.team_batting_id !== this.scorecard.team.id) {
        innings = inn;
        break;
      }
    }
    if (innings) {
      let bowlers = innings.bowl.sort((bowl1, bowl2) => {
        let wkts1 = parseInt(bowl1.wickets, 10);
        let runs1 = parseInt(bowl1.runs, 10);
        let wkts2 = parseInt(bowl2.wickets, 10);
        let runs2 = parseInt(bowl2.runs, 10);
        if (wkts1 > wkts2) {
          return -1;
        } else if (wkts1 === wkts2) {
          if (runs1 > runs2) {
            return 1;
          } else if (runs1 === runs2) {
            return 0;
          } else {
            return -1;
          }
        } else {
          return 1;
        }
      });
      return bowlers.slice(0, no);
    }
    return null;
  }

  getBestBatting(no) {
    let innings = null;
    for (let inn of this.scorecard.innings) {
      if (inn.team_batting_id === this.scorecard.team.id) {
        innings = inn;
        break;
      }
    }
    let bats = null;
    if (innings) {
      bats = innings.bat
        .filter(bat => {
          return bat.how_out !== 'did not bat';
        })
        .sort((bat1, bat2) => {
          let runs1 = parseInt(bat1.runs, 10);
          let runs2 = parseInt(bat2.runs, 10);

          return runs2 - runs1;
        });
      return bats.slice(0, no);
    }
    return null;
  }
}

export default Scorecard;
