import moment from 'moment';

class Matches {
  constructor(matches, clubId) {
    this.clubId = clubId;
    this.matches = matches.map(match => {
      return transformMatch(match, clubId);
    });
  }

  get() {
    return this.matches;
  }

  getLatest() {
    const teams = [];
    const matchDate = moment().add(-4, 'days');

    const matchesOut = this.matches.filter(match => {
      //if (match.match_date_time.isBetween(startDate, endDate)) {
      if (match.match_date_time.isSame(matchDate, 'day')) {
        teams.push(match[`${match.venue}_team_id`]);
        return true;
      }
      return false;
    });

    matchesOut.sort((m1, m2) => {
      if (m1.team.name > m2.team.name) {
        return 1;
      } else {
        return -1;
      }
    });
    return matchesOut;
  }
}

export default Matches;

export function transformMatch(match, clubId) {
  match.match_date_time = moment(
    `${match.match_date} ${match.match_time}`,
    'DD/MM/YYYY HH:mm',
  );
  match.venue = parseInt(match.home_club_id, 10) === clubId ? 'home' : 'away';
  const oppo = match.venue === 'home' ? 'away' : 'home';
  match.oppo_short = oppo;
  match.team = {
    name: match[`${match.venue}_team_name`],
    id: match[`${match.venue}_team_id`],
  };
  match.oppo = {
    club: match[`${oppo}_club_name`],
    club_id: match[`${oppo}_club_id`],
    team: match[`${oppo}_team_name`],
    team_id: match[`${oppo}_team_id`],
  };
  return match;
}
