import Match from './Match';
import connectMatch from './ReduxMatch';

export default connectMatch(Match);
