import React from 'react';
import PropTypes from 'prop-types';
import Scorecard from './../../lib/api/scorecard';
import './Match.css';
export default class Match extends React.PureComponent {
  static propTypes = {
    match: PropTypes.shape().isRequired,
    scorecard: PropTypes.shape(),
    refresh: PropTypes.number,
    fetchScorecard: PropTypes.func.isRequired,
    config: PropTypes.shape({
      clubId: PropTypes.number,
    }),
  };

  static defaultProps = {
    scorecard: null,
    refresh: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      scorecard: null,
    };
  }

  componentDidMount() {
    this.props.fetchScorecard(this.props.match.id);
    if (this.props.refresh) {
      setInterval(() => {
        this.props.fetchScorecard(this.props.match.id);
      }, this.props.refresh);
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.scorecard !== this.props.scorecard) {
      this.setState({
        scorecard: new Scorecard(
          this.props.scorecard,
          this.props.config.clubId,
        ),
      });
    }
  }

  renderScorecard(scorecardObj) {
    let scorecard = scorecardObj.get();
    let batting = scorecardObj.getBestBatting(2);
    let bowling = scorecardObj.getBestBowling(2);
    return (
      <div className="match-details-summary">
        <div className="innings">
          {scorecard.innings.map(inn => {
            return (
              <div key={inn.team_batting_id}>
                <div className="team-name">{inn.team_batting_name}</div>
                <div className="score">
                  {inn.runs} - {inn.wickets}
                  {inn.declared && 'd'} ({inn.overs})
                </div>
                <div className="performances">
                  {inn.team_batting_id === scorecard.team.id && (
                    <div>
                      {batting.map(bat => {
                        return (
                          <div key={bat.position}>
                            {bat.batsman_name} {bat.runs}
                            {bat.how_out === 'no' && '*'}
                          </div>
                        );
                      })}
                    </div>
                  )}
                  {inn.team_batting_id !== scorecard.team.id && (
                    <div>
                      {bowling.map((bowl, i) => {
                        return (
                          <div key={i}>
                            {bowl.bowler_name} {bowl.wickets}-{bowl.runs}
                          </div>
                        );
                      })}
                    </div>
                  )}
                </div>
              </div>
            );
          })}
        </div>
        <div className="match-footer">
          {scorecard.result_description && (
            <div className="result">{scorecard.result_description}</div>
          )}
          <div className="toss">{scorecard.toss}</div>
        </div>
      </div>
    );
  }

  render() {
    let match = this.props.match;
    return (
      <div className="match-summary">
        <div className="team-ven-date">
          {match.team.name} - {match.venue}{' '}
          {match.match_date_time.format('DD/MM/YYYY HH:mm')}
        </div>
        {this.state.scorecard && this.renderScorecard(this.state.scorecard)}
      </div>
    );
  }
}
