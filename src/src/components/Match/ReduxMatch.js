import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchScorecard } from './../../store/actions/app';

const mapStateToProps = state => ({
  config: state.app.config,
  refresh: state.app.matchRefresh,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchScorecard,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
);
