import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMatches } from './../../store/actions/app';

const mapStateToProps = state => ({
  ...state.data,
  config: state.app.config,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchMatches,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
);
