import Home from './Home';
import connectHome from './ReduxHome';

export default connectHome(Home);
