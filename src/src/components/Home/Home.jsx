import React from 'react';
import PropTypes from 'prop-types';
import Matches from './../../lib/api/matches';
import Match from './../Match';
import './Hone.css';

export default class Home extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      matches: null,
    };
  }

  static propTypes = {
    fetchMatches: PropTypes.func.isRequired,
    matches: PropTypes.arrayOf(PropTypes.shape()),
    scorecards: PropTypes.shape(),
    config: PropTypes.shape({
      clubId: PropTypes.number,
    }),
  };

  static defaultProps = {
    scorecards: {},
  };

  componentDidMount() {
    this.props.fetchMatches();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.matches !== this.props.matches) {
      this.setState({
        matches: new Matches(this.props.matches, this.props.config.clubId),
      });
    }
  }

  render() {
    let matches;
    if (this.state.matches === null) {
      return <div>Loading</div>;
    } else {
      matches = this.state.matches.getLatest();
    }

    return (
      <div className="matches-grid">
        {matches.map(match => {
          return (
            <Match
              key={match.id}
              match={match}
              scorecard={
                Object.keys(this.props.scorecards).indexOf(
                  match.id.toString(),
                ) >= 0
                  ? this.props.scorecards[match.id.toString()]
                  : null
              }
            />
          );
        })}
      </div>
    );
  }
}
